var LocalStrategy = require('passport-local').Strategy;  
var FacebookStrategy = require('passport-facebook').Strategy;  
var TwitterStrategy = require('passport-twitter').Strategy;  
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;  
var User = require('../models/modelUsers.js');  
var configAuth = require('./auth.js');  

module.exports = function(passport) {  
	passport.serializeUser(function(user, done) {
    	done(null, user.id);
	});
	passport.deserializeUser(function(id, done) {
		User.findById(id, function(err, user) {
      		done(err, user);
    	});
  	});

	passport.use(new TwitterStrategy({
        consumerKey: configAuth.twitterAuth.consumerKey,
        consumerSecret: configAuth.twitterAuth.consumerSecret,
        callbackURL: configAuth.twitterAuth.callbackURL
    }, function(token, tokenSecret, profile, done) {
	    process.nextTick(function() {	  
       	User.findOne({ 'twitter_id': profile.id }, function(err, user) {
	        if (err) return done(err);
	        if (user) {
	        	return done(null, user);
	        } else {
				var newUser = new User();
				newUser.twitter_id = profile.id;
				newUser.first_name = profile.displayName ? profile.displayName : 'Sin nombre';
				newUser.profile_photo = profile.photos ? profile.photos[0].value : '/assets/img/avatars/adam-jansen.jpg';
				newUser.save(function(err) {
		            if (err)
		              throw err;
		            return done(null, newUser);
				});
			}
	     });
	   });
    }));
	
	passport.use(new FacebookStrategy({  
	    clientID: configAuth.facebookAuth.clientID,
	    clientSecret: configAuth.facebookAuth.clientSecret,
	    callbackURL: configAuth.facebookAuth.callbackURL,
	    enableProof: true,
	    profileFields: ['id', 'email', 'first_name', 'last_name', 'picture.type(large)' ],
	  },
	  function(token, refreshToken, profile, done) {
	    process.nextTick(function() {
			User.findOne({ 'facebook_id': profile.id }, function(err, user) {
	        if (err) return done(err);
	        if (user) {
	        	return done(null, user);
	        } else {
	        	console.log('Cargo modelo nuevo');
				var newUser = new User();
				newUser.facebook_id = profile.id;
				newUser.token = token;
				newUser.first_name = profile.name.givenName ? profile.name.givenName : 'Sin nombres';
				newUser.last_name = profile.name.familyName ? profile.name.familyName : 'Sin apellido';
				newUser.mail = profile.emails ? profile.emails[0].value.toLowerCase() : '';
				newUser.profile_photo = profile.photos ? profile.photos[0].value : '/assets/img/avatars/adam-jansen.jpg';
				newUser.save(function(err) {
		            if (err)  throw err;
		            return done(null, newUser);
				});
			}
	      });
	    });
	}));
	
	passport.use(new GoogleStrategy({
        clientID        : configAuth.googleAuth.clientID,
        clientSecret    : configAuth.googleAuth.clientSecret,
        callbackURL     : configAuth.googleAuth.callbackURL,
    },
    function(token, refreshToken, profile, done) {
        process.nextTick(function() {
            User.findOne({ 'google_id' : profile.id }, function(err, user) {
                if (err) return done(err);
                if (user) {
                    return done(null, user);
                } else {
                    var newUser = new User();
                    newUser.google_id    = profile.id;
                    newUser.token = token;
                    newUser.first_name = profile.displayName ? profile.displayName : '';
    				newUser.mail = (profile.emails[0].value || '').toLowerCase();
    				newUser.profile_photo = profile.photos ? profile.photos[0].value : '/assets/img/avatars/adam-jansen.jpg';
                    newUser.save(function(err) {
                        if (err) throw err;
                        return done(null, newUser);
                    });
                }
            });
        });

    }));


}	

