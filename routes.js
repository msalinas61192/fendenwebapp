var express = require('express'),
    router = express.Router(),
    path = require("path"),
    bodyParser = require("body-parser"),
    passport = require('passport'),
    ctrlOffer = require('./controllers/ctrlOffer.js'),
    ctrlUser = require('./controllers/ctrlUser.js'),
    ctrlCategories = require('./controllers/ctrlCategories.js'),
    ctrlComments = require('./controllers/ctrlComments.js'),
    ctrlMessage = require('./controllers/ctrlMessages.js');
    
//Configuracion body parser
router.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
router.use(bodyParser.json({ limit: '50mb' }));

//Enrutamiento carpetas
router.use('/assets', express.static('public/assets')); //Carpeta de recursos
router.use('/pages', express.static('public/pages/')); //Carpeta de templates angular
router.use('/.well-known', express.static('public/.well-known/')); //Carpeta de templates angular
router.use((err,req, res, next) => {
	res.status(err.status || 500);
	res.render('error',{ message : err.message, error : err });
});

//Pagina Principal
router.get('/', function (req, res) {
    if(req.session.object_id != null){
      params = {  
    	  id : req.session.object_id,
		  name : req.session.first_name+' '+req.session.last_name,
		  mail : req.session.mail,
		  avatar : req.session.profile_photo  };
      res.render('index',params);
    } else {
      res.sendFile(path.join(__dirname+'/public/login.html'));
    }
});

//Cerrar Sesion
router.get('/logout', function (req, res) {
   req.session.destroy();
   res.redirect("/");
});

//Implementación de login con facebook para nodejs
router.get('/auth/facebook', passport.authenticate('facebook'));
router.get('/auth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/' }),(req, res)  => {
    ctrlUser.socialUser_facebook(req,res);
});

//Implementacion de login con Google para NodeJS
router.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));
router.get('/auth/google/callback',passport.authenticate('google', { failureRedirect: '/' }),(req, res) => {
    ctrlUser.socialUser_google(req,res);
});

//Implementacion de login con twitter para nodejs
router.get('/auth/twitter', passport.authenticate('twitter'));
router.get('/auth/twitter/callback', passport.authenticate('twitter',{ failureRedirect: '/' }),(req, res) => {
    ctrlUser.socialUser_twitter(req,res);
});


/**
 * Controladores de ofertas
 * */
//Subir Oferta
router.post('/offerup',(req, res) => {  
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    ctrlOffer.addOffer(req,res);
});
//Lista de ofertas
router.post('/listOffers',(req, res) => {  
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    ctrlOffer.listOffers(req,res);
});


//Lista de ofertas
router.post('/listCategoryOffers',(req, res) => {  
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
	ctrlOffer.listCategoryOffers(req,res);
});

//Obtener Informacion de Oferta
router.post('/offerData',(req, res) => {  
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    ctrlOffer.offerData(req,res);
});

//Likes y Dislikes
router.post('/like',(req, res) => {  
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    ctrlOffer.like(req,res);
});

//Obtener Informacion de Oferta
router.post('/addComment',(req, res) => {  
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    ctrlComments.addComment(req,res);
});

//Lista de Comentario
router.post('/listComments',(req, res) => {  
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    ctrlComments.listComments(req,res);
});

//Informacion de la categoria
router.post('/infoCategory',(req, res) => {  
    ctrlCategories.infoCategory(req,res);
});

router.post('/profileUserInfo',(req, res) => {  
    ctrlUser.profileUser(req,res);
});

//Agregar Comentario a perfil
router.post('/addCommentProfile',(req, res) => {
    ctrlComments.addCommentProfile(req,res);
});

//Lista de Comentarios de perfil
router.post('/listCommentsProfile',(req, res) => {  
    ctrlComments.listCommentsProfile(req,res);
});


//Lista de Categorias
router.get('/listCategories',(req, res) => {  
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    ctrlCategories.listCategories(req,res);
});

//getImage
router.get('/offerImage/:id/:num',(req, res) => {  
	ctrlOffer.offerImage(req,res);
});

router.post('/follow',(req, res) => {  
    ctrlUser.followUser(req,res);
});

router.post('/sendMessage',(req, res) => {  
	ctrlMessage.sendMessage(req,res);
});


router.post('/listConversations',(req, res) => {  
	ctrlMessage.listConversations(req,res);
});

router.post('/profileMessage',(req, res) => {  
	ctrlUser.profileMessage(req,res);
});

router.post('/listMessage',(req, res) => {  
	ctrlMessage.listMessage(req,res);
});

router.post('/readMessages',(req, res) => {  
	ctrlMessage.readMessages(req,res);
});

router.post('/newMessage',(req, res) => {  
	ctrlMessage.newMessage(req,res);
});

router.post('/lastMessages',(req, res) => {  
	ctrlMessage.lastMessages(req,res);
});

router.post('/mark',(req, res) => {  
	ctrlOffer.mark(req,res);
});

router.post('/nextOffer',(req, res) => {  
	switch(req.body.type) {
    	case 'home':
    		ctrlOffer.nextOffer(req,res);
        	break;
    	case 'category':
    		break;
	}
});

router.post('/listOffersNearby',(req, res) => {  
	ctrlOffer.listOffersNearby(req,res);
});

router.post('/sendComplaint',(req, res) => {  
	ctrlUser.sendComplaint(req,res);
});


//URL para angularjs
router.all('/*',(req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
	if(req.session.object_id != null){
      params = {  
    	  id : req.session.object_id,
		  name : req.session.first_name+' '+req.session.last_name,
		  mail : req.session.mail,
		  avatar : req.session.profile_photo  };
      res.render('index',params);
    } else {
      res.sendFile(path.join(__dirname+'/public/login.html'));
    }
});

module.exports = router