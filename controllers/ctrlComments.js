var ModelComment = require('../models/modelComments.js'); //Modelo de oferta
module.exports = {
	addComment : function(request,response){
		var datos_comment = {
			offerId: request.body.id,
		    commentary: request.body.commentary,
		    postedBy: (request.session.object_id)?request.session.object_id:request.body.user_id
	  	};
		//Si la imagen no esta vacia se guarda
		if(request.body.image != undefined) datos_comment.image = request.body.image;
		if(request.body.thumb != undefined) datos_comment.thumb = request.body.thumb;
    	
        var comment = new ModelComment(datos_comment); 
		
		comment.save(function(err,data) {
	            if (err){
	            	response.setHeader('Content-Type', 'application/json');
					response.send(JSON.stringify({ 'success' : false }));
	            } else {	            	
	            	var datos = {
						'id' : data.id,
					    'commentary' : request.body.commentary,
					    'postedBy' : {
							'profile_photo' : request.session.profile_photo,
							'first_name' : request.session.first_name,
					    	'last_name' : request.session.last_name
					    }
				  	};
	            	if(request.body.thumb != undefined)	datos.thumb = request.body.thumb;
				  	
	            	response.setHeader('Content-Type', 'application/json');
					response.send(JSON.stringify({ 'success' : true, 'data' : datos }));
	            }
		});
	},
	listComments : function(request,response){
		ModelComment.find(
			{ status : 1, offerId : request.body.id },
			null,
			{ skip: 0, limit: 10, sort : { date : -1 } })
			.populate('postedBy','profile_photo first_name last_name image')
			.exec(function(err,datos){
				console.log(datos);
				if (err) return handleError(err);
				if(datos != null){
					
					var data = new Array;
					for (var i=0; i < datos.length; i++) {
					 	data.push({
							'id' : datos[i]._id,
							'commentary' : datos[i].commentary,
							'thumb' : (datos[i].thumb != undefined ? datos[i].thumb : ''),
							'date' : datos[i].date,
							'postedBy' : datos[i].postedBy,
							'date' : datos[i].date
						});
					}
					
                	response.setHeader('Content-Type', 'application/json');
    				response.send(JSON.stringify({ 'success' : true , 'data' : data }));
				} else {
                	response.setHeader('Content-Type', 'application/json');
    				response.send(JSON.stringify({ 'success' : false }));
				}
		});
	},
	addCommentProfile : function(request,response){
		var datos_comment = {
			profileId : request.body.id,
		    commentary: request.body.commentary,
		    postedBy: request.session.object_id
	  	};
		//Si la imagen no esta vacia se guarda
		if(request.body.image != undefined) datos_comment.image = request.body.image;
    	
        var comment = new ModelComment(datos_comment); 
		
		comment.save(function(err,data) {
	            if (err){
	            	response.setHeader('Content-Type', 'application/json');
					response.send(JSON.stringify({ 'success' : false }));
	            } else {	            	
	            	var datos = {
						'id' : data.id,
					    'commentary' : request.body.commentary,
					    'postedBy' : {
							'profile_photo' : request.session.profile_photo,
							'first_name' : request.session.first_name,
					    	'last_name' : request.session.last_name
					    }
				  	};
	            	if(request.body.image != undefined)	datos.image = request.body.image;
				  	
	            	response.setHeader('Content-Type', 'application/json');
					response.send(JSON.stringify({ 'success' : true, 'data' : datos }));
	            }
		});
	},
	listCommentsProfile : function(request,response){
		ModelComment.find(
			{ status : 1, profileId : request.body.id },
			null,
			{ skip: 0, limit: 10, sort : { date : -1 } })
			.populate('postedBy','profile_photo first_name last_name image')
			.exec(function(err,datos){
				console.log(datos);
				if (err) return handleError(err);
				if(datos != null){
					
					var data = new Array;
					for (var i=0; i < datos.length; i++) {
					 	data.push({
							'id' : datos[i]._id,
							'commentary' : datos[i].commentary,
							'image' : (datos[i].image != undefined ? datos[i].image : ''),
							'date' : datos[i].date,
							'postedBy' : datos[i].postedBy,
							'date' : datos[i].date
						});
					}
					
                	response.setHeader('Content-Type', 'application/json');
    				response.send(JSON.stringify({ 'success' : true , 'data' : data }));
				} else {
                	response.setHeader('Content-Type', 'application/json');
    				response.send(JSON.stringify({ 'success' : false }));
				}
		});
	},
}