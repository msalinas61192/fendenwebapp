var ModelOffer = require('../models/modelOffers.js'),
			fs = require('fs'),
			Q = require('q'),
			mongoose = require('mongoose'),
			assert = require('assert');

module.exports = {
	addOffer : function(request,response){
		var images_pre = [ request.body.imagen1, request.body.imagen2, request.body.imagen3  ], images = [];
		for (var i=0; i < images_pre.length; i++) {
		  if(images_pre[i]!=undefined) images.push(images_pre[i]);
		};
		
		var thumb_pre = [ request.body.thumb1, request.body.thumb2, request.body.thumb3  ], thumbs = [];
		for (var i=0; i < thumb_pre.length; i++) {
		  if(thumb_pre[i]!=undefined) thumbs.push(thumb_pre[i]);
		};
		
		var datos_offer = {
            title : request.body.ofertaTitulo,
    		price : request.body.ofertaPrecio,
		    shop : request.body.ofertaTienda,
		    branch : request.body.ofertaSucursal,
		    start_date : request.body.ofertaInicio,
		    end_date : request.body.ofertaFin,
		    commentary : request.body.ofertaComentario,
		    address : request.body.ofertaUbicacion,
		    thumbnails : thumbs,
		    images : images,
			latitude : (isNaN(request.body.latitude))?parseFloat(request.body.latitude.replace(',','.')):parseFloat(request.body.latitude),
			longitude : (isNaN(request.body.longitude))?parseFloat(request.body.longitude.replace(',','.')):parseFloat(request.body.longitude),
		    postedBy : (request.session.object_id)?request.session.object_id:request.body.user_id,
		    category : request.body.category
		};
		
		var offer = new ModelOffer(datos_offer);
		var promise = offer.save();
	    assert.ok(function(){
	    	response.json({ 'success' : (err ? false : true) });
	    });

		promise.then(function (doc) {
			response.json({ 'success' : true, 'id' : doc._id });
		}).catch(err => {
			response.json({ 'success' : false });
	    });
	},
	mark : function(request,response){
		ModelOffer.update({ _id :  request.body.id },{ mark : true }, { strict: false }, function(error) {
			console.log('Destacada');
		});
	},
	listOffers : function(request,response){
		var filter = { status : 1 };
		if(request.body.filter != undefined)
			filter.title = new RegExp(request.body.filter, 'i');

		ModelOffer.find(
				filter,
				'title price shop branch start_date end_date commentary address status date thumbnails postedBy category dislikes likes',
				{ skip: 0, limit: 6, sort : { date : -1 } })
				.populate('postedBy category likes.Users dislikes.Users')
				.exec(function(err,datos){
					if (err) return handleError(err);
					if(datos != null){
						var data = new Array;						
						for (var i=0; i < datos.length; i++) {
							data.push({
							  	id : datos[i]._id,
							  	title : datos[i].title,
		    					price : datos[i].price,
		    					shop : datos[i].shop,
		    					branch : datos[i].branch,
		    					start_date : datos[i].start_date,
		    					end_date : datos[i].end_date,
		    					commentary : datos[i].commentary,
		    					address : datos[i].address,
		    					date : datos[i].date,
		    					thumbnail : datos[i].thumbnails[0],
		    					postedBy : datos[i].postedBy,
		    					category : { icon : datos[i].category.classes, name : datos[i].category.category, path : datos[i].category.path_categories },
		    					dislikes : (datos[i].dislikes.indexOf(request.session.object_id) != -1 ? true : false ),
		    					likes : (datos[i].likes.indexOf(request.session.object_id) != -1 ? true : false ),
		    					dislikes_count : datos[i].dislikes.length,
		    					likes_count :  datos[i].likes.length
							});
						};
	    				response.json({ 'success' : true , 'data' : data });
					} else {
	                	response.json({ 'success' : false });
					}
		});
	},
	listCategoryOffers : function(request,response){
		var filter = { status : 1, category : request.body.id };
		ModelOffer.find(filter,
			'title price shop branch start_date end_date commentary address status date thumbnails postedBy category dislikes likes',
			{ skip: 0, limit: 7, sort : { date : -1 } })
			.populate('postedBy category')
			.exec(function(err,datos){
				if (err) return handleError(err);
				if(datos != null){
					var data = new Array;
					for (var i=0; i < datos.length; i++) {
					  data.push({
					  	id : datos[i]._id,
					  	title : datos[i].title,
    					price : datos[i].price,
    					shop : datos[i].shop,
    					branch : datos[i].branch,
    					start_date : datos[i].start_date,
    					end_date : datos[i].end_date,
    					commentary : datos[i].commentary,
    					address : datos[i].address,
    					date : datos[i].date,
    					thumbnail : datos[i].thumbnails[0],
    					postedBy : datos[i].postedBy,
    					category : { icon : datos[i].category.classes, name : datos[i].category.category },
    					dislikes : (datos[i].dislikes.indexOf(request.session.object_id) != -1 ? true : false ),
    					likes : (datos[i].likes.indexOf(request.session.object_id) != -1 ? true : false ),
    					dislikes_count : datos[i].dislikes.length,
    					likes_count :  datos[i].likes.length
					  })
					};
                	response.json({ 'success' : true , 'data' : data });
				} else {
                	response.json({ 'success' : false });
				}
		});
		
	
	},
	offerData : function(request,response){		
		ModelOffer.find(
			{ status : 1, _id : request.body.id },
			'title price shop branch start_date end_date commentary latitude longitude address status date thumbnails postedBy',
			{ skip: 0, limit: 1, sort : { date : -1 } })
			.populate('postedBy','profile_photo first_name last_name')
			.exec(function(err,datos){
				if (err) return handleError(err);
				if(datos != null){
					var data = new Array;
					for (var i=0; i < datos.length; i++) {
					  data.push({
					  	id : datos[i]._id,
					  	title : datos[i].title,
    					price : datos[i].price,
    					shop : datos[i].shop,
    					branch : datos[i].branch,
    					start_date : datos[i].start_date,
    					end_date : datos[i].end_date,
    					commentary : datos[i].commentary,
    					address : datos[i].address,
    					date : datos[i].date,
    					thumbnails :  datos[i].thumbnails,
    					postedBy : datos[i].postedBy,
    					latitude : datos[i].latitude,
						longitude : datos[i].longitude
					  })
					};
                	response.json({ 'success' : true , 'data' : data });
				} else {
                	response.json({ 'success' : false });
				}
		});
	},
	offerImage : function(request,response){	
		ModelOffer.find(
			{ status : 1 , _id : request.params.id },
			'images')
			.exec(function(err,datos){
				if (err) return handleError(err);
				if(datos != null){
					try {
						var data = datos[0].images[parseInt(request.params.num)].replace(/^data:image\/\w+;base64,/, "");
						var buf = new Buffer(data, 'base64');
						response.setHeader('Content-Type', 'image/jpeg');
					} catch (err) {
						var data = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';
						var buf = new Buffer(data, 'base64');
						response.setHeader('Content-Type', 'image/gif');
					}
					response.send(buf);
				} else {
                	response.json({ 'success' : false });
				}
		});
	},
	nextOffer : function(request,response){	
		ModelOffer.find(
				{ status : 1, date: { $lt : request.body.date } },
				'title price shop branch start_date end_date commentary address status date thumbnails postedBy category dislikes likes',
				{ skip: 0, limit: 5, sort : { date : -1 } }) 
				.populate('postedBy category likes.Users dislikes.Users')
				.exec(function(err,datos){
					if (err) return handleError(err);
					if(datos != null){
						var data = new Array;
						for (var i = 0; i < datos.length; i++) {
							data.push({
							  	id : datos[i]._id,
							  	title : datos[i].title,
		    					price : datos[i].price,
		    					shop : datos[i].shop,
		    					branch : datos[i].branch,
		    					start_date : datos[i].start_date,
		    					end_date : datos[i].end_date,
		    					commentary : datos[i].commentary,
		    					address : datos[i].address,
		    					date : datos[i].date,
		    					thumbnail : datos[i].thumbnails[0],
		    					postedBy : datos[i].postedBy,
		    					category : { icon : datos[i].category.classes, name : datos[i].category.category, path : datos[i].category.path_categories },
		    					dislikes : (datos[i].dislikes.indexOf(request.session.object_id) != -1 ? true : false ),
		    					likes : (datos[i].likes.indexOf(request.session.object_id) != -1 ? true : false ),
		    					dislikes_count : datos[i].dislikes.length,
		    					likes_count :  datos[i].likes.length
							});
						}
	                	response.json({ 'success' : true , 'data' : data });
					} else {
	                	response.json({ 'success' : false });
					}
		});
		
	},
	listOffersNearby : function(request,response){
		//Genera una lista de ofertas cercanas
		var lat = parseFloat(request.body.lat);
		var lng = parseFloat(request.body.lng);
		const aprox = 0.04;
		
		ModelOffer.find({ latitude : { '$gte': lat-aprox, '$lt': lat+aprox }, longitude : { '$gte': lng-aprox, '$lt': lng+aprox } },
				'title category latitude longitude',
				{ skip: 0, limit: 10, sort : { date : -1 } }).exec(function(err,datos){
					if (err) console.log(err);
					if(datos != null){
	    				response.json(datos);
					} else {
	                	response.json({ 'success' : false });
					}
		});		
	},
	like : function(request,response){
		//Valida si ya se habia hecho like o dislike con anterioridad
		var searchLikesOnArray = function(){
			var deferred = Q.defer();
			var params = { 'likes' : (request.session.object_id)?request.session.object_id:request.body.user_id,  _id : request.body.post_id };
			console.log(request.body);
			if(request.body.type == 'dislike')
				params = { 'dislikes' : (request.session.object_id)?request.session.object_id:request.body.user_id,  _id : request.body.post_id };
			ModelOffer.count(params, function(err,data){
				deferred.resolve(data);
			});
			return deferred.promise;
		}
		
		//Eliminar la opcion opuesta si  se selecciono
		var removePrevious = function(){
			var deferred = Q.defer();
			var params = { $pull: { 'dislikes' : request.session.object_id } };
			if(request.body.type == 'dislike')
				params = { $pull: { 'likes' : request.session.object_id } };
				
			ModelOffer.update({ _id : request.body.post_id },
				params, function(err,data){
				deferred.resolve(true);
			});
			return deferred.promise;
		}
		
		//Hacer una opcion selecionada
		var makeLike = function(){
			console.log('Ejecuta MakeLike');
			var params = { likes : (request.session.object_id)?request.session.object_id:request.body.user_ids };
			if(request.body.type == 'dislike')
				params = { dislikes : (request.session.object_id)?request.session.object_id:request.body.user_id };
			ModelOffer.findOneAndUpdate(
	    		    { _id : request.body.post_id },
	    		    { $push: params },
	    		    { safe: true, upsert: true },
	    		    function(err, model) {
	    		    	if (err) return handleError(err); 	
	  					if(model != null){
	  						response.json({ 'success' : true, 'type' : request.body.type });
	  					} else {
	  						response.json({ 'success' : false });
	  					}
	    		    });
		}
		
		
		/*
		 * Las funciones especificadas se ejecutan mediante una Q promise 
		 * Se realizan las validaciones de cada funcion
		 * */
		searchLikesOnArray().then(function(data){
			console.log(data);
			if(data){
				return false
			} else {
				return removePrevious();
			}
		}).then(function(data){
			if(data){
				makeLike();
			} else {
				response.json({ 'success' : false });
			}
		});
		
    }
}