var ModelMessage = require('../models/modelMessages.js'),
        mongoose = require('mongoose'),
        session = require('express-session'),
        Q = require('q');

module.exports = {
		readMessages : function(request,response){
			ModelMessage.update({ _id: { $in : request.body.lista_mensajes  } }, { $set: { read : true } }, { multi: true }, function (err, data) {
				if (err) return handleError(err);
				response.send(data);
			});
		},
		sendMessage : function(request,response){
			var datos_message = {
					message : request.body.message,
				    from : request.session.object_id,
				    to : request.body.id
			};
			
			if(request.body.image != undefined) datos_message.image = request.body.image;
			
			var message = new ModelMessage(datos_message);
			
			message.save(function(err,data) {
	            if (err){
	            	response.setHeader('Content-Type', 'application/json');
					response.send(JSON.stringify({ 'success' : false }));
	            } else {	
	            	var datos = {
	            			date : data.date,
	            			message : data.message,
	            			from : { profile_photo : request.session.profile_photo }
	            	};
	         
	            	
	            	response.setHeader('Content-Type', 'application/json');
					response.send(JSON.stringify({ 'success' : true, 'data' : datos }));
	            }
			});
		},
		listMessage : function(request,response){
			for (var y = 0; y < 20; y++) {
				console.log();
			}
			console.log([ { 'to': request.session.object_id }, { 'from' : request.body.user_id } ]);
			ModelMessage.find({
		        	$and : [
		        		{ $or : [ { to: request.body.user_id }, { to : request.session.object_id } ] },
		        		{ $or : [ { from : request.body.user_id }, { from : request.session.object_id } ] }
		        		]
					},
					null,
					{ sort : { date : 1 } })
					.populate('to','profile_photo first_name last_name image')
					.populate('from','profile_photo first_name last_name image')
					.exec(function(err,datos){
						console.log(datos);
						if (err) return handleError(err);
						if(datos != null){
		                	response.setHeader('Content-Type', 'application/json');
		    				response.send(JSON.stringify({ 'success' : true , 'data' : datos }));
						} else {
		                	response.setHeader('Content-Type', 'application/json');
		    				response.send(JSON.stringify({ 'success' : false }));
						}
			});
			
		},
		newMessage : function(request,response){
			ModelMessage.findOneAndUpdate(
					{ to : request.session.object_id, from : request.body.user_id, read : false },
					{ $set : { read : true } },
					{ sort : { date : 1 } })
					.populate('to','profile_photo first_name last_name image')
					.populate('from','profile_photo first_name last_name image')
					.exec(function(err,datos){
						if (err) return handleError(err);
						if(datos != null){
		                	response.setHeader('Content-Type', 'application/json');
		    				response.send(JSON.stringify({ 'success' : true , 'data' : datos }));
						} else {
		                	response.setHeader('Content-Type', 'application/json');
		    				response.send(JSON.stringify({ 'success' : false }));
						}
			});
		
		},
		lastMessages : function(request,response){

			ModelMessage.find({ to : request.session.object_id },
					null,
					{ sort : { date : 1 }, limit : 3 , skip: 0 })
					.populate('from','profile_photo first_name last_name image')
					.exec(function(err,datos){
						if (err) return handleError(err);
						if(datos != null){
		                	response.setHeader('Content-Type', 'application/json');
		    				response.send(JSON.stringify({ 'success' : true , 'data' : datos }));
						} else {
		                	response.setHeader('Content-Type', 'application/json');
		    				response.send(JSON.stringify({ 'success' : false }));
						}
			});
			
		},
		listConversations : function(request,response){			
			/**
			 * Las funciones getToMessage y getFromMessage permiten obtener la lista de personas 
			 * con las que se obtuvo actividad
			 * 
			 * */
			
			
			function getToMessage(){
				var deferred = Q.defer();
				ModelMessage.find({ 'from' : request.session.object_id }).distinct('to', function(error, data) {
					deferred.resolve(data);
				});
				return deferred.promise;
			}
			
			
			function getFromMessage(){
				var deferred = Q.defer();
				ModelMessage.find({ 'to' : request.session.object_id }).distinct('from', function(error, data) {
					deferred.resolve(data);
				});
				return deferred.promise;
			}
			
			//Mediante una promesa se espera la obtención de las 2 consultas
			Q.all([
				getToMessage(),
				getFromMessage()
			]).then(function(data) {
				var arrayPre  = data[0].concat(data[1]);
				response.setHeader('Content-Type', 'application/json');
				response.send(JSON.stringify({ 'success' : true, 'data' : arrayPre.unique() }));
			});
		}
}