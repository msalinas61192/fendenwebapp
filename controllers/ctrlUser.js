var ModelUser = require('../models/modelUsers.js'),
        mongoose = require('mongoose'),
        session = require('express-session'),
        email = require('mailer'),
        nodemailer = require('nodemailer'); 
        
module.exports = {
        socialUser_facebook: (request,response) => {
        	var datos_login = { 'facebook_id' : request.user.facebook_id };
        	ModelUser.findOne(datos_login,'_id rank first_name profile_photo last_name mail birthdate phone_number followed',function(err,datos){
                  if (err) return handleError(err);
					  if(datos != null){
					      request.session.object_id = datos.id;
					      request.session.first_name = datos.first_name;
					      request.session.last_name = (datos.last_name != undefined) ? datos.last_name : '';
					      request.session.mail = (datos.mail != undefined ?  datos.mail : '');
					      request.session.profile_photo = datos.profile_photo;
					      request.session.rank = datos.rank;
					      request.session.followed = datos.followed;
					      request.session.save();
					      
					      //Validar si se ejecutar desde escritorio
					      if(request.device.type.toUpperCase() == 'DESKTOP'){
					    	  response.redirect('/');
					      } else {
					    	  var objUserFB = {
								    			 'facebook_id' : request.user.facebook_id,
								    			 'user_id' : datos.id,
								    			 'first_name' : datos.first_name,
								    			 'last_name' : (datos.last_name != undefined) ? datos.last_name : '',
								    			 'profile_photo' : datos.profile_photo,
								    			 'rank' : datos.rank };

					    	  response.send(JSON.stringify(objUserFB));
					      }
					  }
            });
        },
        socialUser_twitter: (request,response) => {
        	var datos_login = { 'twitter_id' : request.user.twitter_id };
        	ModelUser.findOne(datos_login,'_id rank first_name profile_photo mail birthdate followed',function(err,datos){
                  if (err) return handleError(err);
					  if(datos != null){
					      request.session.object_id = datos.id;
					      request.session.first_name = datos.first_name;
					      request.session.last_name = (datos.last_name != undefined) ? datos.last_name : '';
					      request.session.profile_photo = datos.profile_photo;
					      request.session.rank = datos.rank;
					      request.session.followed = datos.followed;
					      request.session.save();
					      if(request.device.type.toUpperCase() == 'DESKTOP'){
					    	  response.redirect('/');
					      } else { 
					    	  var objUserTwitter = {
						    			 'twitter_id' : request.user.twitter_id,
						    			 'user_id' : datos.id,
						    			 'first_name' : datos.first_name,
						    			 'last_name' : (datos.last_name != undefined) ? datos.last_name : '',
						    			 'profile_photo' : datos.profile_photo,
						    			 'rank' : datos.rank };

					    	  response.send(JSON.stringify(objUserTwitter));
					      }                		  
					  }
            });
        },
        socialUser_google: (request,response) => {
        	var datos_login = { 'google_id' : request.user.google_id };
        	ModelUser.findOne(datos_login,'_id rank first_name profile_photo mail followed',function(err,datos){
                  if (err) return handleError(err);
					  if(datos != null){
					      request.session.object_id = datos.id;
					      request.session.first_name = datos.first_name;
					      request.session.last_name = (datos.last_name != undefined) ? datos.last_name : '';
					      request.session.profile_photo = datos.profile_photo;
					      request.session.rank = datos.rank;
					      request.session.followed = datos.followed;
					      request.session.save();

					      if(request.device.type.toUpperCase() == 'DESKTOP'){
					    	  response.redirect('/');
					      } else { 
						    	  var objUserGoogle = {
							    			 'google_id' : request.user.twitter_id,
							    			 'user_id' : datos.id,
							    			 'first_name' : datos.first_name,
							    			 'last_name' : (datos.last_name != undefined) ? datos.last_name : '',
							    			 'profile_photo' : datos.profile_photo,
							    			 'rank' : datos.rank };

						    	  response.send(JSON.stringify(objUserGoogle));
						    	  
						  } 
					  }
            });
        },
        profileUser: (request,response) => {
        	 ModelUser.findOne({ _id : request.body.id },'profile_photo mail last_name first_name date description followed',function(err,datos){
                 if (err) return handleError(err);
					  if(datos != null){
						  console.log(datos);
						  response.setHeader('Content-Type', 'application/json');
						  response.send(JSON.stringify({ 'success' : true , 'data' : datos, }));
					  } else {
						  response.setHeader('Content-Type', 'application/json');
						  response.send(JSON.stringify({ 'success' : false }));
					  }
             });
        },
        followUser: (request,response) => {
        	ModelUser.findOneAndUpdate(
        		    { _id: request.session.object_id },
        		    { $push: { followed : request.body.user_follower }},
        		    { safe: true, upsert: true },
        		    function(err, model) {
        		    	if (err) return handleError(err);
	  					if(model != null){
	  						response.setHeader('Content-Type', 'application/json');
	  						response.send(JSON.stringify({ 'success' : true}));
	  					} else {
	  						response.setHeader('Content-Type', 'application/json');
	  						response.send(JSON.stringify({ 'success' : false }));
	  					}
        		    });
        },
        profileMessage : (request,response) => {
        	 ModelUser.findOne({ _id : request.body.id },'profile_photo last_name first_name',function(err,datos){
                 if (err) return handleError(err);
					  if(datos != null){
						  response.setHeader('Content-Type', 'application/json');
						  response.send(JSON.stringify({ 'success' : true , 'data' : datos, }));
					  } else {
						  response.setHeader('Content-Type', 'application/json');
						  response.send(JSON.stringify({ 'success' : false }));
					  }
             });
        },
        sendComplaint :  (request,response) => {

	    	response.setHeader('Content-Type', 'application/json');
        	var transporter = nodemailer.createTransport({
        	    host: 'smtp.gmail.com',
        	    port: 465,
        	    secure: true, // use SSL
        	    auth: {
        	        user: 'msalinas61192@gmail.com',
        	        pass: 'M5SysLog.,'
        	    }
        	});
        	
        	var mailOptions = {
        	    from: '"Matias Salinas" <msalinas61192@gmail.com>', // sender address (who sends)
        	    to: 'msalinas61192@gmail.com', // list of receivers (who receives)
        	    subject: 'Denuncia Perfil : '+request.body.id, // Subject line
        	    text: 'Denuncia Perfil : '+request.body.id, // plaintext body
        	    html: request.body.complaint // html body
        	};

        	// send mail with defined transport object
        	transporter.sendMail(mailOptions, function(error, info){
        	    if(error) response.send(JSON.stringify({ success : false }));        	    
        	    
				response.send({ success : true });
        	});
        }
}