var ModelCategory = require('../models/modelCategories.js'); //Modelo de oferta
module.exports = {
	addCategory : function(request,response){
		var datos_category = { 
			path_categories : request.body.path,
    		category : request.body.name,
    		classes : request.body.classes
	  	};
        var category= new ModelCategory(datos_category); 
		category.save(function(err) {
	            if (err)
	            	response.json({ 'success' : false });
	            else
	            	response.json({ 'success' : true });
		});
	},
	listCategories : function(request,response){
		ModelCategory.find({ status : 1 },'path_categories category classes')
			.exec(function(err,datos){
				if (err) return handleError(err);
				
				if(datos != null)
					response.json({ 'success' : true , 'data' : datos});
				else
					response.json({ 'success' : false });
		});
	},
	infoCategory : function(request,response){		
		ModelCategory.find({ status : 1, path_categories : request.body.nombre },'_id path_categories category classes')
			.exec(function(err,datos){
				if (err) return handleError(err);
				if(datos != null)
					response.json({ 'success' : true , 'data' : datos});
				else
					response.json({ 'success' : false });
		});
	}
}