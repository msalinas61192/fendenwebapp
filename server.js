//Modulo de base de datos
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/fenden');

var express = require('express'),
app = express(),
cookieParser = require('cookie-parser'),
https = require('https'),
fs = require('fs'),
device = require('express-device');

const session = require('express-session');


var passport = require('passport');  
var LocalStrategy = require('passport-local').Strategy;  

/*
 * Añade funcion unique a prototype de array nativo
 * */
Array.prototype.unique= function () {
  return this.reduce(function(previous, current, index, array){
     previous[current.toString()+typeof(current)]=current;
     return array.length-1 == index ? Object.keys(previous).reduce(function(prev,cur){
          prev.push(previous[cur]);
          return prev;
       },[]) : previous;
   }, {});
};

//Inicializa App
app.set('view engine', 'jade');
app.use(cookieParser());
app.use(device.capture());

/*
 * 	 app.use(function(req, res, next) {
 *   	res.header("Access-Control-Allow-Origin", "*");
 *   	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
 *   	next();
 *   });
 *   
 *   Habilita acceso a consultas desde cualquier sitio
 *   Se habilita para desarrollar vistas desde angular2
*/


app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
}));
app.use(passport.initialize());  
app.use(passport.session());  
require('./config/passport.js')(passport);

//Establece enrutamiento
app.use(require('./routes.js'));


/**
 * app.listen(80);
 * Nos permite pasar la aplicación exclusivamente a puerto 80
 * Pero se debe comentar las lineas que levantan el servidor por puerto 80 y 443
 * 
 * */

//Servidor HTTPS
https.createServer({
      /*key: fs.readFileSync('/etc/letsencrypt/live/fendenapp.com/privkey.pem'),
      cert: fs.readFileSync('/etc/letsencrypt/live/fendenapp.com/fullchain.pem'),
      ca: fs.readFileSync('/etc/letsencrypt/live/fendenapp.com/cert.pem') **/
	  key: fs.readFileSync('../llaves/privkey.pem'),
      cert: fs.readFileSync('../llaves/fullchain.pem'),
      ca: fs.readFileSync('../llaves/cert.pem')
}, app).listen(443,function(){
	console.log('Servidor HTTPS puerto 443');
});

// Redireccion a https
var http = require('http');
http.createServer(function (req, res) {
    res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
    res.end();
}).listen(80, function(){
	console.log('Servidor HTTP puerto 80');
});


