//Modelo de Mensajes
var mongoose = require('mongoose');   
var messageSchema = new mongoose.Schema({
    message : { type: String },
    image : { type: String },
    read : { type: Boolean, default: false },
    from : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users'
    },
    to : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users'
    },
    date: { type: Date, default: Date.now }
}, { collection: 'collMessages' });  
module.exports = mongoose.model('Message',messageSchema); 