//Modelo de usuarios
var mongoose = require('mongoose');   
var commentSchema = new mongoose.Schema({
    commentary : { type: String },
    thumb : { type: String },
    image : { type: String },
    status : { type: Boolean, default: true },
    offerId : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Offers'
    },
    postedBy : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users'
    },
    profileId : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users'
    },
    date: { type: Date, default: Date.now }
}, { collection: 'collComments' });  
module.exports = mongoose.model('Comments',commentSchema); 