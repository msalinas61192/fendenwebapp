//Modelo de usuarios
var mongoose = require('mongoose');  
var categoriesSchema = new mongoose.Schema({
    path_categories : { type: String },
    category : { type: String },
    classes : { type: String },
    status : { type: Boolean, default: true }
}, { collection: 'collCategories' });  
module.exports = mongoose.model('Categories', categoriesSchema); 