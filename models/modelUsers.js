//Modelo de usuarios
var mongoose = require('mongoose');  
var userSchema = new mongoose.Schema({  
    first_name: {type: String, required: true},
    last_name: {type: String, required: false},
    password: {type: String, required: false},
    birthdate: {type: Date, required: false},
    mail: {type: String, required: false},
    profile_photo : { type: String, required: false},
    facebook_id: {type: String, required: false},
    twitter_id: {type: String, required: false},
    google_id: {type: String, required: false},
    phone_number: {type: String, required: false},
    token: {type: String, required: false},
    rank:{type: Number, default:1} ,
    date: { type: Date, default: Date.now },
    followed : [ { type: mongoose.Schema.Types.ObjectId } ]
}, { collection: 'collUsers' });  
module.exports = mongoose.model('Users', userSchema); 