//Modelo de usuarios
var mongoose = require('mongoose');
var userSchema = new mongoose.Schema({  
    title: {type: String, required: true},
    price: {type: String, required: true},
    shop: {type: String, required: true},
    branch: {type: String, required: false},
    start_date: { type: Date }, 
    end_date: { type: Date },
    commentary : { type: String },
    address : { type: String },
    status : { type: Boolean, default: true },
    mark : { type: Boolean, default: false },
	latitude : { type: Number },
	longitude : { type: Number },
    likes : [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users'
    }],
    dislikes : [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users'
    }],
    postedBy : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users'
    },
    category : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Categories'
    },
    thumbnails : [ { type: String, default: '/assets/img/thumbnail.png' } ],
    images : [ { type: String } ],
    date: { type: Date, default: Date.now }
}, { collection: 'collOffers' });  
module.exports = mongoose.model('Offers', userSchema); 