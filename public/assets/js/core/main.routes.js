'use strict';
var appFenden = angular.module("AppAngular", ['ngRoute','infinite-scroll']);

appFenden.config(['$routeProvider','$locationProvider',function($routeProvider,$locationProvider) {
	$locationProvider.hashPrefix('');
	$routeProvider
    	.when('/', {
			templateUrl : 'pages/main.html',
			controller  : 'mainController'
    	})
    	.when('/profile/:id', {
			templateUrl : 'pages/profile.html',
	        controller  : 'profileController'
    	})
    	.when('/offer', {
    		templateUrl : 'pages/offer.html',
	        controller  : 'offerController'
    	})
    	.when('/messages/', {
    		templateUrl : 'pages/message.html',
	        controller  : 'messageController'
    	})
    	.when('/more/:id', {
    		templateUrl : 'pages/more.html',
	        controller  : 'moreController'
    	})
    	.when('/category/:nombre', {
    		templateUrl : 'pages/category.html',
	        controller  : 'categoryController'
    	})
    	.when('/map', {
    		templateUrl : 'pages/map.html',
	        controller  : 'mapController'
    	})
    	.otherwise({
            redirectTo: '/'
        });
	$locationProvider.html5Mode(true);
}]);