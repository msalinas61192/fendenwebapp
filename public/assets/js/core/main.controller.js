appFenden.controller('mainController', ['$scope','$http','$location','$window','$timeout',function($scope, $http, $location, $window, $timeout) {
	$scope.goToMap = function(){ 
		console.log('Ir al mapa');
		$location.url('/map');
	}
	
	$scope.listOffer = new Array;
	$scope.listId = new Array;
	
	$scope.loadlistOffers = function(filter){
		$scope.listOffer = new Array; //Vacia el array
		filter = (typeof filter === 'undefined') ? {} : { filter : filter };
		
		console.log(filter);
		$http({
	      method  : 'POST',
	      url     : '/listOffers',
	      data : filter
	    })
	    .then(function successCallback(response) {
			var data = response.data;
	     	if(data.success){
	     		for (var i=0; i < data.data.length; i++) {
	     			$scope.listId.push(data.data[i].id);
	     			var url = 'https://www.facebook.com/sharer.php?caption='+data.data[i].title+'&description='+data.data[i].commentary+'&u=https://fendenapp.com/more/'+data.data[i].id+'&picture='+(data.data[i].thumbnail != undefined ? 'https://fendenapp.com/offerImage/'+data.data[i].id+'/0' : '');
	     			var  nombre_usuario = 'Usuario no existe';
	     			if(data.data[i].postedBy != null){
	     				nombre_usuario  = data.data[i].postedBy.first_name+' '+(data.data[i].postedBy.last_name != undefined ? data.data[i].postedBy.last_name : '');
	     			}
	     			
	     			var title = data.data[i].title;
	     			if(data.data[i].title.length>=33){
	     				title = data.data[i].title.substring(0,30)+'...';
     				}
	     			
	     			var commentary = data.data[i].commentary;
	     			if(data.data[i].commentary.length>=160){
	     				commentary = data.data[i].commentary.substring(0,157)+'...';
     				}
	     			
	     			$scope.listOffer.push({ 
				 		id : data.data[i].id,
				 		title : title,
						price : data.data[i].price,
						shop : data.data[i].shop,
						branch : data.data[i].branch,
						start_date : moment(data.data[i].start_date).format("DD-MM-YYYY"),
						end_date : moment(data.data[i].end_date).format("DD-MM-YYYY"),
						commentary : commentary,
						address : data.data[i].address,
						image: data.data[i].thumbnail,
				 		avatar: (data.data[i].postedBy != null?data.data[i].postedBy.profile_photo:'/assets/img/generic-avatar.png'),
				 		user_id: (data.data[i].postedBy != null?data.data[i].postedBy._id:''),
				 		name: nombre_usuario,
				 		category : data.data[i].category,
			     		share_facebook : function(){
			     			$window.open(url, "popup", "width=300,height=200,left=10,top=150");
			     		},
		     			share_twitter : 'https://twitter.com/intent/tweet?text='+data.data[i].title+'&url=https://fendenapp.com/more/'+data.data[i].id,
		     			share_google : 'https://plus.google.com/share?url=https://fendenapp.com/more/'+data.data[i].id,
		     			likes : data.data[i].likes,
		     			dislikes : data.data[i].dislikes,
		     			likes_count : data.data[i].likes_count,
		     			dislikes_count : data.data[i].dislikes_count
				 	});	
				 	$scope.lastDate = data.data[data.data.length-1].date;
				} 
	     	}
	   });
	}
	
	$scope.loadlistOffers();
	
	$scope.loadReady = true;
	$scope.loadMore = function() {
		if($scope.loadReady){
			var filter = { date : $scope.lastDate, type : 'home' };

			if(angular.element('#textFilter').val() != '') filter.filter = angular.element('#textFilter').val();
			
			console.log(filter);
			$scope.loadReady = false;
			if($scope.lastDate != undefined){
				$http({
				      method  : 'POST',
				      url     : '/nextOffer',
				      data : filter
				}).then(function successCallback(response) {
					var data = response.data;
					for (var i=0; i < data.data.length; i++) {
						if($scope.listId.indexOf(data.data[i].id) == -1){
			     			$scope.listId.push(data.data[i].id);
			     			var url = 'https://www.facebook.com/sharer.php?caption='+data.data[i].title+'&description='+data.data[i].commentary+'&u=https://fendenapp.com/more/'+data.data[i].id+'&picture='+(data.data[i].thumbnail != undefined ? 'https://fendenapp.com/offerImage/'+data.data[i].id+'/0' : '');
			     			
			     			var title = data.data[i].title;
			     			if(data.data[i].title.length>=33){
			     				title = data.data[i].title.substring(0,30)+'...';
		     				}
			     			
			     			var commentary = data.data[i].commentary;
			     			if(data.data[i].commentary.length>=160){
			     				commentary = data.data[i].commentary.substring(0,157)+'...';
		     				}
			     			
			     			$scope.listOffer.push({ 
						 		id : data.data[i].id,
						 		title : title,
								price : data.data[i].price,
								shop : data.data[i].shop,
								branch : data.data[i].branch,
								start_date : new Date(data.data[i].start_date).toISOString().slice(0,10),
								end_date : new Date(data.data[i].end_date).toISOString().slice(0,10),
								commentary : commentary,
								address : data.data[i].address,
								image: data.data[i].thumbnail,
						 		avatar: data.data[i].postedBy.profile_photo,
						 		user_id: data.data[i].postedBy._id,
						 		name: data.data[i].postedBy.first_name+' '+(data.data[i].postedBy.last_name != undefined ? data.data[i].postedBy.last_name : ''),
						 		category : data.data[i].category,
					     		share_facebook : function(){
					     			$window.open(url, "popup", "width=300,height=200,left=10,top=150");
					     		},
				     			share_twitter : 'https://twitter.com/intent/tweet?text='+data.data[i].title+'&url=https://fendenapp.com/more/'+data.data[i].id,
				     			share_google : 'https://plus.google.com/share?url=https://fendenapp.com/more/'+data.data[i].id,
				     			likes : data.data[i].likes,
				     			dislikes : data.data[i].dislikes,
				     			likes_count : data.data[i].likes_count,
				     			dislikes_count : data.data[i].dislikes_count
						 	});
						 	
						 	$scope.lastDate = data.data[data.data.length-1].date;
						}
					} 
				});
			}
		}
		
		$timeout(function(){
			$scope.loadReady = true;
		},1000);
	};
	
	$scope.searchFilter = function(){
		var textFilter = angular.element('#textFilter').val();
		console.log(textFilter);
	};
	
}]);
