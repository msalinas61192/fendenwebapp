appFenden.controller('profileController',['$scope','$routeParams','$http','$location',function($scope,$routeParams,$http, $location) {
	$http({
	      method  : 'POST',
	      url     : '/profileUserInfo',
	      data	  : { id : $routeParams.id }
	})
	.then(function successCallback(response) {
		var data = response.data;
	    if(data.success){
	    	console.log(data.data);
	    	$scope.profile_info = {
	    			date : data.data.date,
	    			name : ( data.data.first_name != undefined ? data.data.first_name : '' )+( data.data.last_name != undefined ? ' '+data.data.last_name : '' ),
	    			mail : data.data.mail,
	    			profile_photo : data.data.profile_photo,
	    			description : data.data.description
	    	};
	    }
	});
	
	
	$scope.follow = function(){
		$http({
		      method  : 'POST',
		      url     : '/follow',
		      data	  : { user_follower : $routeParams.id }
		})
		.then(function successCallback(response) {
			var data = response.data;
		    if(data.success){
		    	console.log(data.data);
		    }
		});
	};
	
	$scope.comments = new Array;
	$http({
	     method  : 'POST',
	     url     : '/listCommentsProfile',
	     data : { 'id' : $routeParams.id }
	}).then(function successCallback(response) {
		var data = response.data;
     	for (var i=0; i < data.data.length; i++) {
     		var arr_date = data.data[i].date.split('.');
		 	$scope.comments.push({ 
		 		id : data.data[i].id,
		 		avatar : data.data[i].postedBy.profile_photo,
		 		name : data.data[i].postedBy.first_name+((data.data[i].postedBy.last_name != undefined) ? ' '+data.data[i].postedBy.last_name : '' ),
				image : (data.data[i].image != undefined ? data.data[i].image : false),
		 		commentary : data.data[i].commentary,
		 		date : arr_date[0].replace('T',' ')
		 	});
     	}
	});
	
	$scope.addComment = function(){
		$http({
		      method  : 'POST',
		      url     : '/addCommentProfile',
		      data	  : { id : $routeParams.id, commentary : $scope.textComment, 'image' : angular.element('#imageInputComment').val() }
		})
		.then(function successCallback(response) {
			var data = response.data;
	 		$scope.textComment = '';
	 		if(data.success){
			 	$scope.comments.unshift({ 
			 		id: data.data.id,
			 		avatar : data.data.postedBy.profile_photo,
			 		name : data.data.postedBy.first_name+((data.data.postedBy.last_name != undefined) ? ' '+data.data.postedBy.last_name : '' ),
			 		commentary: data.data.commentary
			 	});
	 		} else {
		 		swal("Ocurrio un error al agregar el comentario");
	 		}
		});
	};
	
	
	
	$scope.sendMessage = function(){
		$http({
		      method  : 'POST',
		      url     : '/sendMessage',
		      data	  : { id : $routeParams.id, message : $scope.textMessage }
		})
		.then(function successCallback(response) {
			
		});
	};
	
	$scope.sendComplaint = function(){
		$('#modalDenuncia').modal('hide');
		$http({
		      method  : 'POST',
		      url     : '/sendComplaint',
		      data	  : { id : $routeParams.id, complaint : $scope.textComplaint }
		}).then(function successCallback(response) {
			if(response.data.success){
				swal("Exito!", "Denuncia ingresada exitosamente!", "success");
			} else {
				swal("Error!", "Ocurrio un error al ingresar la denuncia!", "error");
			}
		});
	}
	
    
}]);