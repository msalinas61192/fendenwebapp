appFenden.controller('messageController',['$scope','$routeParams','$http','$location','$interval','$timeout',function($scope,$routeParams,$http, $location, $interval, $timeout) {
	$scope.goToEndChat = function(){
		var element = document.getElementById("chat_area");
		element.scrollTop = element.scrollHeight;
	};
	
	
	$scope.list_profile = new Array;
	$http({
	      method  : 'POST',
	      url     : '/listConversations',
	      data	  : {}
	})
	.then(function successCallback(response) {
		var data = response.data;
		if(data.success){
			for (var y = 0; y < data.data.length; y++) {
				var data_id = data.data[y];
				//Get Profile Info
				$http({
				      method  : 'POST',
				      url     : '/profileMessage',
				      data	  : { id :  data_id }
				})
				.then(function successCallback(response) {
					var data =  response.data;
					console.log(data.data);
					if(data.success){
						$scope.list_profile.push({
							id : data.data._id,
							name : data.data.first_name+(data.data.last_name != undefined ? ' '+data.data.last_name : ''),
							avatar : data.data.profile_photo
						});
					}
				});
				
			}
		}
	});
	
	$scope.listMessages = new Array;
	
	$scope.gotoProfile = function(id,name,avatar){
		$scope.name = name;
		$scope.avatar = avatar;
		$scope.profile_id = id;
		var datos = { user_id : id };
		$http({
		      method  : 'POST',
		      url     : '/listMessage',
		      data	  : datos
		})
		.then(function successCallback(response) {
			var data = response.data;
			if(data.success){
				$scope.listMessages = data.data;
				console.log(data.data);
				var lista_array = new Array;
				for (var i = 0; i < data.data.length; i++) {
					lista_array.push(data.data[i]._id);
				}
				
				//Marcar Lectura
				$http({
				      method  : 'POST',
				      url     : '/readMessages',
				      data	  : { lista_mensajes : lista_array }
				})
				.then(function successCallback(response) {
					var data = response.data;
			 		if(data.success){
			 			console.log(data.data);
			 		}
				});
				$timeout(function(){
		 			$scope.goToEndChat();
				},200);

			}
		});
	};
	
	
	$scope.sendMessage = function(){
		$http({
		      method  : 'POST',
		      url     : '/sendMessage',
		      data	  : { id : $scope.profile_id, message : $scope.textMessage }
		})
		.then(function successCallback(response) {
			var data = response.data;
	 		if(data.success){
	 			$scope.listMessages.push(data.data);
	 			$timeout(function(){
					$scope.goToEndChat();
				},300);
	 		}
		});
	};
	
	$interval(function(){
			if($scope.profile_id != undefined){
				$http({
				      method  : 'POST',
				      url     : '/newMessage',
				      data	  : { user_id : $scope.profile_id }
				})
				.then(function successCallback(response) {
					var data = response.data;
					if(data.success){
						$scope.listMessages.push(data.data);

						$timeout(function(){
							$scope.goToEndChat();
						},300);
					    /*var scroller = document.getElementById("chat_area");
					    scroller.scrollTop = scroller.scrollHeight;*/
					}
				})
			}
	},1000);
	
	
}]);