function modalCategoria(){
	$('#modalCategoria').modal('show');
}


function resizeImage(base64, maxWidth, maxHeight) {
	if(typeof(maxWidth) === 'undefined') var maxWidth = 500;
	if(typeof(maxHeight) === 'undefined') var maxHeight = 500;
		  
	var canvas = document.createElement("canvas");
	var ctx = canvas.getContext("2d");
	var canvasCopy = document.createElement("canvas");
	var copyContext = canvasCopy.getContext("2d");
	
	var img = new Image();
	img.src = base64;
	
	
	
	copyContext.imageSmoothingEnabled = false;
	copyContext.webkitImageSmoothingEnabled = false;
	copyContext.mozImageSmoothingEnabled = false;
	
	var ratio = 1;
	if(img.width > maxWidth)
		ratio = maxWidth / img.width;
	else if(img.height > maxHeight)
	ratio = maxHeight / img.height;
		    
	canvasCopy.width = img.width;
	canvasCopy.height = img.height;
	copyContext.drawImage(img, 0, 0);
		
	canvas.width = img.width * ratio;
	canvas.height = img.height * ratio;
	ctx.drawImage(canvasCopy, 0, 0, canvasCopy.width, canvasCopy.height, 0, 0, canvas.width, canvas.height);
		
	return canvas.toDataURL('image/jpeg', 0.9);
}